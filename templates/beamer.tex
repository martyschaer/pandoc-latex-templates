\documentclass[
  ignorenonframetext,
  aspectratio=169,
  handout
  xcolor=dvipsnames
]{beamer}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Packages                                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{float}
\usepackage{siunitx}
\usepackage{setspace}
\usepackage{multicol}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Theme configuration                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usetheme[numbering=progressbar]{focus}

$if(main-color)$
  \definecolor{main}{HTML}{$main-color$}
$else$
  \definecolor{main}{HTML}{435488}
$endif$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Colors                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Links
\definecolor{default-linkcolor}{HTML}{000000}
\definecolor{default-filecolor}{HTML}{000000}
\definecolor{default-citecolor}{HTML}{000000}
$if(main-color)$
    \definecolor{default-urlcolor}{HTML}{$main-color$}
$else$
    \definecolor{default-urlcolor}{HTML}{435488}
$endif$

% Blockquotes
$if(main-color)$
    \definecolor{blockquote-border}{HTML}{$main-color$}
$else$
    \definecolor{blockquote-border}{HTML}{435488}
$endif$

\definecolor{blockquote-text}{RGB}{119,119,119}

% Tables
\definecolor{table-row-color}{HTML}{F5F5F5}
\definecolor{table-rule-color}{HTML}{999999}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Formatting settings                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(strikeout)$
    \usepackage[normalem]{ulem}
    \pdfstringdefDisableCommands{\renewcommand{\sout}{}}
$endif$

\providecommand{\tightlist}{
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}
}

$if(pagestyle)$
    \pagestyle{$pagestyle$}
$endif$

\floatplacement{figure}{H}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Math settings                                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\everymath{\displaystyle}

% SI Units
\sisetup{group-minimum-digits = 4}
\sisetup{group-separator = {'}}

% Relational Algebra
\DeclareRobustCommand{\ojoin}{\rule[-0.0035ex]{.2em}{.4pt}\llap{\rule[1.029ex]{.2em}{.4pt}}}
\newcommand{\naturaljoin}{\bowtie}
\newcommand{\leftouterjoin}{\mathrel{\:\!\ojoin\mkern-5.9mu\naturaljoin}}
\newcommand{\rightouterjoin}{\mathrel{\naturaljoin\mkern-5.9mu\ojoin\:\!}}
\newcommand{\fullouterjoin}{\mathrel{\:\!\ojoin\mkern-5.9mu\naturaljoin\mkern-5.9mu\ojoin\:\!}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Syntax highlighting                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(highlighting-macros)$
    $highlighting-macros$
$endif$

\usepackage{fvextra}
\renewcommand{\theFancyVerbLine}{
    \arabic{FancyVerbLine}
}
\fvset{fontsize=\footnotesize}
\RecustomVerbatimEnvironment{verbatim}{Verbatim}{}
\DefineVerbatimEnvironment{Highlighting}{Verbatim}{
    breaklines,
    breaksymbolleft={},
    breaksymbolright={},
    commandchars=\\\{\},
    xleftmargin=1cm
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Link settings                                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PDF settings
\PassOptionsToPackage{unicode=true}{hyperref}
\hypersetup{
    colorlinks=true,
    urlcolor=default-urlcolor,
    linkcolor=default-linkcolor,
    filecolor=default-filecolor,
    citecolor=default-citecolor
}

% Use same font for URLs
\urlstyle{same}  

% Forcing linebreaks in URLs 
\PassOptionsToPackage{hyphens}{url}

% Make links footnotes instead of hotlinks:
$if(links-as-notes)$
    \DeclareRobustCommand{\href}[2]{#2\footnote{\url{#1}}}
$endif$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Multicolumns                                                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\colorRule}[3][black]{\textcolor[HTML]{#1}{\rule{#2}{#3}}}
\newcommand{\colsbegin}{\begin{columns}}
\newcommand{\colsend}{\end{columns}}
\def\col#1{\column{.#1\textwidth}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Beamer template                                                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setbeamercolor{normal text}{fg=black}
\setbeamercolor{section in toc}{fg=black}
\setbeamercolor{subsection in toc}{fg=black}

% Hide navigation symbolds
\setbeamertemplate{navigation symbols}{}

\setbeamertemplate{caption}[numbered]

\setbeamertemplate{title page}{
  \begin{flushleft}
      % Set line spacing for title page
      \setstretch{1.4}

      % Text color for title page
      \color[HTML]{4F4F4F}

      $if(main-color)$
          \makebox[0pt][l]{\colorRule[$main-color$]{1.3\textwidth}{4pt}}
      $else$
          \makebox[0pt][l]{\colorRule[435488]{1.3\textwidth}{4pt}}
      $endif$
      
      \vskip 2em

      % Title
      {\huge \textbf{\textsf{$title$}}}
      
      % Subtitle
      $if(subtitle)$
          {\Large \textsf{$subtitle$}}
      $endif$

      \vfill

      % Authors
      $if(author)$
          \textsf{
              $for(author)$
                  \phantom{}\hfill $author$\newline
              $endfor$
          }
      $endif$

      \textsf{\hfill $date$}
  \end{flushleft}
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Graphic settings                                                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{graphicx}

\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
\setkeys{Gin}{width=0.8\linewidth,height=0.75\maxheight,keepaspectratio}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Table settings                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{longtable}
\usepackage{booktabs}

\renewcommand{\arraystretch}{1.75}
\setlength\heavyrulewidth{0.3ex}

% Tables
\usepackage{caption}
% These lines are needed to make table captions work with longtable:
\makeatletter
\def\fnum@table{\tablename~\thetable}
\makeatother


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Footnotes                                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\let\oldfootnote\footnote
\renewcommand\footnote[1][]{\oldfootnote[frame,#1]}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Blockquotes                                                                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{mdframed}
\newmdenv[
    rightline=false,
    bottomline=false,
    topline=false,
    linewidth=3pt,
    linecolor=blockquote-border,
    backgroundcolor=background,
    skipabove=\parskip
]{customblockquote}

\renewenvironment{quote}
{
    \begin{customblockquote}
    \itshape
    \list{}{\rightmargin=0em\leftmargin=0em}%
    \item\relax\color{blockquote-text}\ignorespaces
}
{
    \unskip\unskip\endlist\end{customblockquote}
} 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Circuits                                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{tikz}
\usepackage[european]{circuitikz}
\usepackage{pgfplots}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Meta variables                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{$title$}
\author{
    $for(author)$
        $author$,
    $endfor$
}
\date{$date$}


\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Titlepage                                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[plain,t]
  \titlepage
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Table of contents                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]
    \frametitle{\contentsname}
    \begin{multicols}{2}
        \tableofcontents[hideallsubsections]
    \end{multicols}
\end{frame}
\newpage

$body$

\end{document}
