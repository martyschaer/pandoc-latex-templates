---
title: "Pandoc \\LaTeX\\ Template"
subtitle: "Document Example"
author:
    - Severin Kaderli
    - Second Author
lot: true
lof: true
glossary: true
glossary-file: "assets/glossary.tex"
abstract: |
  This is an abstract.

  ## Subtitle
...

# Section

## Subsection

### Subsubsection

#### Examples
 
# Text 
*Italics*

**Bold**

~~Strikethrough~~

# Lists
## Unordered Lists
- First point
- Second point
    - A subpoint
    - Another subpoint
- Third Point

## Ordered Lists
1. First point
2. Second point
    1. A subpoint
    2. Another subpoint
3. Third point
   
# Math
## Inline
This is an inline math equation: $\sum_{i=0}^{n} \frac{a_i}{1+x}$. The text can
continue afterwards. This is even more text to fill up the remaining space that
the equation takes.

## Block

$$
    x = \sum_{i=0}^{n} \frac{a_i}{1+x}
$$

## SI Units
$$
v = \SI{2.3}{m/s}
$$

# Source Code
```{.python .numberLines}
for i in range(10):
    print("Long test to show that there are automatic line wraps in source code blocks when the line is too long.")
```

# Links
## Hyperlinks
You can easily create links like this [one](https://severinkaderli.ch).
 

## Footnotes
Footnotes are also supported.^[They are very simple to use!]

# Images
![Placeholder Image](./assets/placeholder.png)

# Tables
: Placeholder Table

\begin{tab}{5}{Placeholder Table}{1 & 2 & 3 & 4 & 5}
1 & 2 & 3 & 4 & 5 \\ \hline
1 & 2 & 3 & 4 & 5 \\ \hline
1 & 2 & 3 & 4 & 5 \\ \hline
\end{tab}

# Blockquotes
> This is a blockquote.  
> It can span multiple lines!

# Horizontal Ruler

---

# Circuits
\begin{figure}[h!]
  \begin{center}
    \begin{circuitikz}
      \draw(0,0)
      to[generic=A] (0,2)
      to[generic=B] (2,2)
      to[generic=D] (4,2)
      to[generic=E] (4,0)
      to[short] (0,0)
      (2,2) to[generic=C, *-*] (2,0);
    \end{circuitikz}
    \caption{Example Circuit}
  \end{center}
\end{figure}

# Plots
\pgfmathdeclarefunction{gauss}{2}{%
  \pgfmathparse{1/(#2*sqrt(2*pi))*exp(-((x-#1)^2)/(2*#2^2))}%
}
\begin{figure}[h!]
  \begin{center}
    \begin{tikzpicture}
        \begin{axis}[every axis plot post/.append style={
        mark=none,domain=-2:3,samples=50,smooth},
        axis x line*=bottom,
        axis y line*=left,
        enlargelimits=upper]
        \addplot {gauss(0,0.5)};
        \addplot {gauss(1,0.75)};
        \end{axis}
    \end{tikzpicture}
    \caption{Example Plot}
  \end{center}
\end{figure}

